import re
import subprocess
import webbrowser
from typing import NamedTuple

import exceptions

pattern = {"vk.com": ['вк', 'вконтакте', 'вэка'],
            "yotube.com": ['ютуб', 'ютаб', 'видео', 'ютубa']}

class Message(NamedTuple):
    command: str
    state: str

class Task(NamedTuple):
    command: str
    state: str

def open_website(text: str):
    reg_ex = re.search('открыть(.+)', text)
    if not reg_ex:
        return 'no'
    website = reg_ex.group(1)
    for key, value in pattern.items():
        if website.strip() in value:
            url = 'https://www.{}'.format(key)
            webbrowser.open(url)
            return "ok"

def change_volume(text: str):
    if re.findall('уменьши|уменьшить|убавь|минус', text):
        cmd = "nircmd.exe changesysvolume -20000"
        txt = "уменьшаю звук"
    elif re.findall('увеличь|увеличить|прибавь|плюс', text):
        cmd = "nircmd.exe changesysvolume 20000"
        txt = "увеличиваю звук"
    elif re.findall('включи|вруби', text):
        cmd = "nircmd.exe mutesysvolume 0 "
        txt = "включаю звук"
    elif re.findall('выключи|выруби|сон', text):
        cmd = "nircmd.exe mutesysvolume 1"
        txt = "выключаю звук"
    elif re.findall('средне|средний|спокойно', text):
        cmd = "nircmd.exe mutesysvolume 2"
        txt = "звук в спокойный режим"
    subprocess.call(cmd, shell=True)
    return txt

def change_display_brightness(text: str): 
    # cmd =  [r'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe',
    #          f'(Get-WmiObject -Namespace root/WMI -Class WmiMonitorBrightnessMethods).WmiSetBrightness(1, {parsed_message.state})']
    if re.findall('включи|вруби', text):
        cmd = "nircmd.exe setbrightness 100 2 "
        txt = "яркость на максимум"
    elif re.findall('выключи|выруби|сон', text):
        cmd = "nircmd.exe setbrightness 0 2"
        txt = "яркость на минимум"
    elif re.findall('средне|средний|спокойно', text):
        cmd = "nircmd.exe setbrightness 50 2"
        txt = "яркость в средний режим"
    subprocess.call(cmd, shell=True)   
    return txt


def preparing_for_sleep():
    cmd = "nircmd.exe setbrightness 0 2 && nircmd.exe mutesysvolume 1"
    subprocess.call(cmd, shell=True)
    return 'сладких'


def task_menedger(text: str) -> str:
    if re.findall('открыть|открой|зайди|зайти', text):
        return open_website(text)
    elif re.findall('звук|громкость', text):
        return change_volume(text)
    elif re.findall('яркость|монитор', text):
        return change_display_brightness(text)
    elif re.findall('сон|пока|спать', text):
        return preparing_for_sleep()


def _parse_message(raw_message:str) -> Message:
    result = raw_message.split(" ")
    if not result or not result[0] \
            or not result[1]:
        raise exceptions.NotCorrectMessage(
            "Не могу понять команду. Напишите в формате,"
            "например:\nяркость 100")

    command = result[0].strip().lower()
    state = result[1]
    return Message(command=command, state=state)