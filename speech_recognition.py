
from vosk import Model, KaldiRecognizer
import os
from preprocessing_audio import convert_to_pcm16b16000r
import json

class ModelAudio():
    def __init__(self, sample_rate: int, debug: bool):
        self.sample_rate = sample_rate
        self.debug = debug
        

    def created_model(self, language: str):
        self.model  = Model(language)

    def speech_to_text(self, bytes: int) -> json:
        rec = KaldiRecognizer(self.model, self.sample_rate)
        data = convert_to_pcm16b16000r(in_bytes=bytes)
        if rec.AcceptWaveform(data):
            pass
        return rec.FinalResult()
        
    def get_text(self, bytes: int) -> str:
        result_json = self.speech_to_text(bytes)
        text = json.loads(result_json)
        return text['text']
  
        


    