import subprocess
import os
import requests
import logging
import json

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

from dotenv import load_dotenv
from speech_recognition import ModelAudio

import tasks
import exceptions

load_dotenv()
# logging.basicConfig(level=logging.DEBUG)

Model = ModelAudio(16000, debug=False)
Model.created_model("model-ru")

TOKEN = os.getenv("TOKEN")

bot = Bot(token=TOKEN)
dp = Dispatcher(bot)

# @dp.message_handler(commands=['start'])
# async def process_start_command(message: types.Message):
#     await message.reply("Привет!\nНапиши мне что-нибудь!")

# @dp.message_handler(commands=['browser', 'браузер'])
# async def process_open_browser(message: types.Message):
#     subprocess.Popen(["C:/Program Files/Mozilla Firefox/firefox.exe"], shell=True)
#     await message.reply("Запускаю браузер")

# @dp.message_handler(commands=['brightness', 'яркость'])
# async def process_display_brightness(message: types.Message):
#     expenses.change_display_brightness(message.text)
#     await message.reply("Меняю яркость")

# @dp.message_handler(content_types=['text'])
# async def 

@dp.message_handler(content_types=['voice'])
async def voice_processing(message: types.Message):
    file_info = await bot.get_file(message.voice.file_id)
    file = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(TOKEN, file_info.file_path)) 
    try:
        text = Model.get_text(bytes=file.content)
        try:
            await message.reply(text)
            msg = tasks.task_menedger(text)
            await bot.send_message(chat_id=message.chat.id,text=msg)

        except Exception:
            await bot.send_message(chat_id=message.chat.id,text='Голосовое сообщение пустое')
       
    except exceptions.SpeechException:
        await message.reply("Постигла неудача :(")


@dp.message_handler()
async def echo_message(msg: types.Message):
    await bot.send_message(msg.from_user.id, msg.text)

if __name__ == '__main__':
    executor.start_polling(dp)

